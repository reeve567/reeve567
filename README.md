### Hi 👋

I'm a Software Engineer. I'm pretty good with distributed systems like Kubernetes, and learning more every day.

My favorite projects include ones that bring together different technologies, especially when it makes lives easier

Below you can find some of my strengths, and blow that you can find some of my projects :)

#### Languages

Primary: Kotlin, Java, TypeScript

Secondary: C++, C#, JavaScript, Java, Python, 

Learning: Rust, PHP

#### Frameworks

Primary: NextJS, NodeJS, React,

Secondary: Ktor

Learning: Laravel

#### Databases

Primary: InfluxDB, Postgres

Secondary: Neo4J, MongoDB, Redis

#### Other

Primary: Kubernetes, AWS, OCI (Oracle), Kafka, ArgoCD, Helm
